----------------------
Project 1: Sierpinski Gasket
----------------------

The majority of the code can be found in the textbook as indicated in Loudcloud as well as documentation for the WebGL library online. 

```javascript
canvas = document.getElementById("gl-canvas"); 
gl = WebGLUtils.setupWebGL(canvas); 
```
This allowed for WebGL to use the canvas element defined in our HTML document to draw our Sierpinski Gasket. The variable `gl` will recieve the WebGL context. This allows for WebGL to work within Javascript and to use the pipeline as defined by the OpenGL protocol. 

```javascript
var program = initShaders( gl, "shaders/vshader21.glsl", "shaders/fshader21.glsl" );
gl.useProgram( program ); 
```
The program variable will recieve the WebGL program that has the shaders defined in the glsl files. These shaders are written in the OpenGL language, **not Javascript**. the `gl.useProgram( program );`  is a different type of program then the one that is contained in our Javascript files. This program tells WebGL which program it should use for the current state. 

```javascript
var bufferId = gl.createBuffer(); 
gl.bindBuffer( gl.ARRAY_BUFFER, bufferId );
gl.bufferData( gl.ARRAY_BUFFER, flatten(points), gl.STATIC_DRAW );
```

The `bufferId` will allow WebGL to recongnize it as a buffer. It then can allow data to be passed through it and sent to the GPU when we decide we need to draw our objects defined by the data passed into the buffer. 

```javascript
var vPos = gl.getAttribLocation( program, "vPosition" ); 
gl.vertexAttribPointer( vPos, 2, gl.FLOAT, false, 0, 0 );
gl.enableVertexAttribArray( vPos );
```

The variable `vPos` will retrieve the `vPosition` defined in the glsl files. The WebGL context can then locate where its position and can enable the attribute once it has been found. 

```javascript
function render() 
{
    gl.clear( gl.COLOR_BUFFER_BIT );
    gl.drawArrays( gl.POINTS, 0, points.length );
}
```
The render function clears the color buffer from the GPU so that we will have a clean slate if we choose to draw again. The `drawArrays` function can then draw all the points we have placed into the buffer. 




















