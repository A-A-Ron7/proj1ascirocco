
var canvas; // Creates a global variable that will be used to store the canvas element from the HTML file
var gl; // Creates a global variable that will be used to store the WebGL context as an object
var points = []; // Create a global variable that will be used to store all the random points for the sierpinski gasket
var colors = []; // Holds the color vectors and vertices

var NumTimesToSubdivide = 4; // Initializes the recursive counter

window.onload = function init() // // Once the document is loaded, the init function will be executed
{
    canvas = document.getElementById( "gl-canvas" ); // Allows javascript to recognize the canvas tag by its id given
    
    gl = WebGLUtils.setupWebGL( canvas ); // The WebGL context is retrieved and placed in the variable gl

    // If the browser does not recognize WebGL, the browser will use an alert
    // to notify the user they need WebGL to view the content
    if ( !gl ) { alert( "WebGL isn't available" ); }


    // Initialize the vertices of the 3D gasket
    // Four vertices on the unit circle
    // Intial tetrahedron with equal length sides
    var vertices = [
        vec3(  0.0000,  0.0000, -1.0000 ),
        vec3(  0.0000,  0.9428,  0.3333 ),
        vec3( -0.8165, -0.4714,  0.3333 ),
        vec3(  0.8165, -0.4714,  0.3333 )
    ];
    
    divideTetra( vertices[0], vertices[1], vertices[2], vertices[3],
                 NumTimesToSubdivide); // Recursively builds tetrahedron

    //
    //  Configure WebGL
    //
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 1.0, 1.0, 1.0, 1.0 );
    
    // enable hidden-surface removal
    
    gl.enable(gl.DEPTH_TEST);

    //  Load shaders and initialize attribute buffers
    
    var program = initShaders( gl, "vertex-shader", "fragment-shader" );  //  Load shaders and initialize attribute buffers
    gl.useProgram( program ); // Indicates which program WebGl program should be used for the current rendering state
    
    var cBuffer = gl.createBuffer(); // Creates a buffer
    gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer ); // Binds the cBuffer as an array buffer
    gl.bufferData( gl.ARRAY_BUFFER, flatten(colors), gl.STATIC_DRAW ); // Sends the points array as a 32 bit floating point array into the GPU
    
    var vColor = gl.getAttribLocation( program, "vColor" ); // Retrieves the vPosition from the OpenGL language defined in the HTML file
    gl.vertexAttribPointer( vColor, 3, gl.FLOAT, false, 0, 0 ); // Defines the location of the vColor attributes
    gl.enableVertexAttribArray( vColor ); // Enables the vColor attribute

    var vBuffer = gl.createBuffer(); // Creates a buffer
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer ); // Binds the vBuffer as an array buffer
    gl.bufferData( gl.ARRAY_BUFFER, flatten(points), gl.STATIC_DRAW ); // Sends the points array as a 32 bit floating point array into the GPU

    var vPosition = gl.getAttribLocation( program, "vPosition" ); // Retrieves the vPosition from the OpenGL language defined in the HTML file
    gl.vertexAttribPointer( vPosition, 3, gl.FLOAT, false, 0, 0 ); // Defines the location of the vPosition attributes
    gl.enableVertexAttribArray( vPosition ); // Enables the vPosition attribute

    render(); // Draws the Sierpinski Gasket
};

function triangle( a, b, c, color ) // Pushes color vectors and vertices into colors array and points array respectively
{

    // add colors and vertices for one triangle

    var baseColors = [ // initializes the color vectors
        vec3(0.0, 0.0, 1.0),
        vec3(0.0, 1.0, 0.0),
        vec3(1.0, 0.0, 0.0),
        vec3(1.0, 0.0, 1.0)
    ];

    colors.push( baseColors[color] ); // pushes one of the color vectors into the color array
    points.push( a ); // pushes the vertex at point a into the points array
    colors.push( baseColors[color] ); // pushes one of the color vectors into the color array
    points.push( b ); // pushes the vertex at point b into the points array
    colors.push( baseColors[color] ); // pushes one of the color vectors into the color array
    points.push( c ); // pushes the vertex at point c into the points array
}

function tetra( a, b, c, d ) // creates tetrahedron using different vertices
{
    // tetrahedron with each side using
    // a different color
    
    triangle( a, c, b, 0 ); // Calls triangle function and will use the first color vector from baseColors
    triangle( a, c, d, 1 ); // Calls triangle function and will use the second color vector from baseColors
    triangle( a, b, d, 2 ); // Calls triangle function and will use the third color vector from baseColors
    triangle( b, c, d, 3 ); // Calls triangle function and will use the fourth color vector from baseColors
}

function divideTetra( a, b, c, d, count ) // Recursively creates tetrahedron using tetra and triangle functions
{
    // Checks for end of recursion
    if ( count === 0 ) {
        tetra( a, b, c, d );
    }
    
    // find midpoints of sides
    // divide four smaller tetrahedra
    
    else {
        var ab = mix( a, b, 0.5 ); // Performs linear interpolation between vectors a and b
        var ac = mix( a, c, 0.5 ); // Performs linear interpolation between vectors a and c
        var ad = mix( a, d, 0.5 ); // Performs linear interpolation between vectors a and d
        var bc = mix( b, c, 0.5 ); // Performs linear interpolation between vectors b and c
        var bd = mix( b, d, 0.5 ); // Performs linear interpolation between vectors b and d
        var cd = mix( c, d, 0.5 ); // Performs linear interpolation between vectors c and d

        --count; // Decrements recursion counter

        // The following function are recursive calls that continue the building of the Sierpinski Gasket
        divideTetra(  a, ab, ac, ad, count );
        divideTetra( ab,  b, bc, bd, count );
        divideTetra( ac, bc,  c, cd, count );
        divideTetra( ad, bd, cd,  d, count );
    }
}


function render() // Draws the 3D Sierpinski Gasket
{
    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT); // Clears the Color or Depth Buffer
    gl.drawArrays( gl.TRIANGLES, 0, points.length ); // Draws all triangles from the points array
}
