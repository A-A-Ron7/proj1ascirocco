
var gl; // Creates a global variable that will be used to store the WebGL context as an object
var points; // Create a global variable that will be used to store all the random points for the sierpinski gasket

var NumPoints = 10000; // The amount of points that the algorithm will generate

window.onload = function init() // Once the document is loaded, the init function will be executed
{
    canvas = document.getElementById( "gl-canvas");  // Allows javascript to recognize the canvas tag by its id given
    
    gl = WebGLUtils.setupWebGL( canvas ); // The WebGL context is retrieved and placed in the variable gl

    // If the browser does not recognize WebGL, the browser will use an alert
    // to notify the user they need WebGL to view the content
    if ( !gl ) { alert( "WebGL isn't available" ); }

    var vertices = [ // The points (x,y,0) of the initial triangle
        vec2( -1, -1 ),
        vec2(  0,  1 ),
        vec2(  1, -1 )
    ];

    // Next, generate the rest of the points, by first finding a random point
    //  within our gasket boundary.  We use Barycentric coordinates
    //  (simply the weighted average of the corners) to find the point

    var coeffs = vec3( Math.random(), Math.random(), Math.random() ); // Creates a 3 dimensional vector with random points
    coeffs = normalize( coeffs ); // normalize the coeffs vector

    var a = scale( coeffs[0], vertices[0] ); // returns a point that scales the first vertex by the first coefficient
    var b = scale( coeffs[1], vertices[1] ); // returns a point that scales the second vertex by the second coefficient
    var c = scale( coeffs[2], vertices[2] ); // returns a point that scales the third vertex by the third coefficient

    var p = add( a, add(b, c) ); // Adds the 3 random points together to form a single point p


    points = [ p ]; // Adds point p into the points array (global)

    for ( var i = 0; points.length < NumPoints; ++i ) { // loops until the points array length is equal to that of NumPoints
        var j = Math.floor(Math.random() * 3); // Chooses a random number between 0-2 and places the result into j

        p = add( points[i], vertices[j] ); // Adds the previous point in the points array to a random vertice
        p = scale( 0.5, p ); // Finds the midpoint between points[i] and vertices[j]
        points.push( p ); // Adds point p to the end of the points array
    }

    // sets the WebGL viewport to the dimensions of the canvas elemented
    // defined in the HTML file
    gl.viewport( 0, 0, canvas.width, canvas.height );

    gl.clearColor( .5, .8, 7.0, .85 ); // Creates a background of the defined color
    
    //  Load shaders and initialize attribute buffers
    var program = initShaders( gl, "shaders/vshader21.glsl",
                               "shaders/fshader21.glsl" );
    gl.useProgram( program ); // Indicates which program WebGl program should be used for the current rendering state

    var bufferId = gl.createBuffer(); // Creates a buffer
    gl.bindBuffer( gl.ARRAY_BUFFER, bufferId ); // Binds the bufferId as an array buffer
    gl.bufferData( gl.ARRAY_BUFFER, flatten(points), gl.STATIC_DRAW ); // Sends the points array as a 32 bit floating point array into the GPU

    var vPos = gl.getAttribLocation( program, "vPosition" ); // Retrieves the vPosition from the OpenGL language define in the glsl file
    gl.vertexAttribPointer( vPos, 2, gl.FLOAT, false, 0, 0 ); // Defines the location of the vPos attributes
    gl.enableVertexAttribArray( vPos ); // Enables the vPos attribute

    render(); // Draws the Sierpinski Gasket
};

function render()
{
    gl.clear( gl.COLOR_BUFFER_BIT );
    gl.drawArrays( gl.POINTS, 0, points.length );
}
