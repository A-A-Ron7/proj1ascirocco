
var canvas; // Creates a global variable that will be used to store the canvas element from the HTML file
var gl; // Creates a global variable that will be used to store the WebGL context as an object

var points = []; // Create a global variable that will be used to store all the random points for the sierpinski gasket

var numTimesToSubdivide = 0; // Initializes the recursive counter

function init()
{
    canvas = document.getElementById( "gl-canvas" ); // Allows javascript to recognize the canvas tag by its id given
    
    gl = WebGLUtils.setupWebGL( canvas ); // The WebGL context is retrieved and placed in the variable gl

    // If the browser does not recognize WebGL, the browser will use an alert
    // to notify the user they need WebGL to view the content
    if ( !gl ) { alert( "WebGL isn't available" ); }


    var vertices = [ // The points (x,y,0) of the initial triangle
        vec2( -1, -1 ),
        vec2(  0,  1 ),
        vec2(  1, -1 )
    ];

    // Recursively builds the Sierpinski Gasket using the slider input as the fourth parameter
    divideTriangle( vertices[0], vertices[1], vertices[2],
                    numTimesToSubdivide);

    // sets the WebGL viewport to the dimensions of the canvas elemented
    // defined in the HTML file
    gl.viewport( 0, 0, canvas.width, canvas.height );

    gl.clearColor( 1.0, 1.0, 1.0, 1.0 ); // Creates a solid white background

    //  Load shaders and initialize attribute buffers
    
    var program = initShaders( gl, "vertex-shader", "fragment-shader" ); //  Load shaders and initialize attribute buffers
    gl.useProgram( program ); // Indicates which program WebGl program should be used for the current rendering state

    // Load the data into the GPU
    
    var bufferId = gl.createBuffer(); // Creates a buffer
    gl.bindBuffer( gl.ARRAY_BUFFER, bufferId ); // Binds the bufferId as an array buffer
    gl.bufferData( gl.ARRAY_BUFFER, 50000, gl.STATIC_DRAW ); // Sends the points array as a 32 bit floating point array into the GPU
    gl.bufferSubData(gl.ARRAY_BUFFER, 0, flatten(points)); // Updates a subset of the buffer's object data (in this case, the points array)

    // Associate out shader variables with our data buffer
    
    var vPosition = gl.getAttribLocation( program, "vPosition" ); // Retrieves the vPosition from the OpenGL language define in the glsl file
    gl.vertexAttribPointer( vPosition, 2, gl.FLOAT, false, 0, 0 ); // Defines the location of the vPos attributes
    gl.enableVertexAttribArray( vPosition ); // Enables the vPos attribute

        // Event handler function that will change numTimesToSubdivide depending on the position of the slider
        document.getElementById("slider").onchange = function() {
        numTimesToSubdivide = event.srcElement.value;
    };


    render(); // Draws the 2D Sierpinski Gasket
};

// function pushes all vertices into the points array
function triangle( a, b, c )
{
    points.push( a, b, c );
}

function divideTriangle( a, b, c, count ) //
{

    // Checks for end of recursion
    if ( count === 0 ) {
        triangle( a, b, c );
    }
    else {
    
        // Bisect the sides
        var ab = mix( a, b, 0.5 ); // Performs linear interpolation between vectors a and b
        var ac = mix( a, c, 0.5 ); // Performs linear interpolation between vectors a and c
        var bc = mix( b, c, 0.5 ); // Performs linear interpolation between vectors b and c

        --count; // Decrements recursion counter

        // Creates three new triangles that continue to build the Sierpinski Gasket
        divideTriangle( a, ab, ac, count );
        divideTriangle( c, ac, bc, count );
        divideTriangle( b, bc, ab, count );
    }
}

window.onload = init; // When the document loads, execute the init function

function render() // Draws the 2D Sierpinski Gasket
{
    gl.clear( gl.COLOR_BUFFER_BIT );  // Clears the Color Buffer
    gl.drawArrays( gl.TRIANGLES, 0, points.length ); // Draws the triangles using the vertices in the points array
    points = []; // Clears the points array from memory
    requestAnimFrame(init); // Updates when the user changes the input on the slider
}

